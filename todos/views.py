from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ToDoListForm

# Create your views here.


def show_TodoList(request):
    todos = TodoList.objects.all()
    context = {
        "show_TodoList": todos,
    }
    return render(request, "todos/list.html", context)


def TodoList_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {"todo_detail": todo_detail}
    return render(request, "todos/details.html", context)


def create_TodoList(request):
    if request.method == "POST":
        form = ToDoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = ToDoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_TodoList(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ToDoListForm(request.POST, instance=todo_detail)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = ToDoListForm(instance=todo_detail)

    context = {
        "todo_object": todo_detail,
        "form": form,
    }
    return render(request, "todos/edit.html", context)
