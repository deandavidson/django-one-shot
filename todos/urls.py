from django.urls import path
from todos.views import (
    show_TodoList,
    TodoList_detail,
    create_TodoList,
    edit_TodoList,
)

urlpatterns = [
    path("", show_TodoList, name="todo_list_list"),
    path("<int:id>/", TodoList_detail, name="todo_list_detail"),
    path("create/", create_TodoList, name="todo_list_create"),
    path("<int:id>/edit/", edit_TodoList, name="todo_list_update"),
]
